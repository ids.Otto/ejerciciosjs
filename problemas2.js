function ejercicio1() {
	const personas = [
		{
			personaId: 123,
			name: 'Amelia',
			city: 'Sydney',
			phoneNo: '1234567890',
		},
		{
			personaId: 124,
			name: 'jhon',
			city: 'Melbourne',
			phoneNo: '1234567890',
		},
		{
			personaId: 125,
			name: 'Emily',
			city: 'Perth',
			phoneNo: '1234567890',
		},
		{
			personaId: 126,
			name: 'Abraham',
			city: 'Perth',
			phoneNo: '1234567890',
		},
	];

	const bodyTable = document.querySelector('#bodyTable');

	personas.map(persona => {
		const trTable = document.createElement('tr');
		const tdId = document.createElement('td');
		const tdName = document.createElement('td');
		const tdCity = document.createElement('td');
		const tdPhone = document.createElement('td');

		tdId.textContent = persona.personaId;
		tdName.textContent = persona.name.toUpperCase();
		tdCity.textContent = persona.city.toUpperCase();
		tdPhone.textContent = persona.phoneNo;

		bodyTable.appendChild(trTable);
		trTable.appendChild(tdId);
		trTable.appendChild(tdName);
		trTable.appendChild(tdCity);
		trTable.appendChild(tdPhone);
	});
}

ejercicio1();

function ejercicio2() {
	const usa = document.querySelector('#C1');
	const canada = document.querySelector('#C2');
	const francia = document.querySelector('#C3');

	const mostrarAtributos = elemento => {
		elemento.addEventListener('click', () => {
			const atributoDataId = elemento.getAttribute('data-id');
			const atributoDataDial =
				elemento.getAttribute('data-dial-code');
			const atributoId = elemento.getAttribute('id');
			alert(
				`ID elemento: ${atributoId}\nISO ID: ${atributoDataId}\nDial Code: ${atributoDataDial}`,
			);
		});
	};

	mostrarAtributos(usa);
	mostrarAtributos(canada);
	mostrarAtributos(francia);
}
ejercicio2();
