function problema1() {
	const clientes = ['cliente1', 'cliente2', 'cliente3'];
	const empleados = ['empleado1', 'empleado2', 'empleado3'];

	const contactos1 = clientes.concat(empleados);
	const contactos2 = clientes.join(empleados);
	const contactos3 = clientes.splice(empleados);
	console.log(contactos1);
	console.log(contactos2);
	console.log(clientes.push(empleados));
	console.log(contactos3);

	/* Respuesta: El método más adecuado es .concat ya que une ambos arreglos, .join los mostraría como cadena */
}

/* problema1(); */

function problema2() {
	const numbers = [5, 32, 3, 4];
	const resultado = numbers.filter(n => n % 2 !== 0);

	console.log(resultado);

	/* Respuesta: la variable resultado guarda los números impares */
}

/* problema2(); */

function problema3() {
	const people = [
		{
			id: 1,
			name: 'John',
			age: 28,
		},
		{
			id: 2,
			name: 'Jane',
			age: 31,
		},
		{
			id: 3,
			name: 'Peter',
			age: 55,
		},
		{
			id: 4,
			name: 'Paul',
			age: 60,
		},
		{
			id: 5,
			name: 'Gween',
			age: 23,
		},
		{
			id: 4,
			name: 'Mary',
			age: 36,
		},
	];

	const menorA35 = arreglo =>
		console.log(arreglo.filter(age => age.age < 35));

	menorA35(people);
}

/* problema3(); */

function problema4() {
	const people = [
		{ id: 1, name: 'bob' },
		{ id: 2, name: 'bob' },
		{ id: 3, name: 'john' },
		{ id: 4, name: 'peter' },
		{ id: 5, name: 'john' },
		{ id: 6, name: 'bob' },
	];
	const contarRepetidos = arreglo => {
		const repetidos = [];
		const objetoRep = [];

		for (let i = 0; i < arreglo.length; i++) {
			if (!repetidos.includes(arreglo[i].name)) {
				repetidos.push(arreglo[i].name);
				objetoRep.push({
					nombre: arreglo[i].name,
					nVeces: 1,
				});
			} else
				objetoRep.forEach(n =>
					n.nombre === arreglo[i].name ? n.nVeces++ : null,
				);
		}
		console.log(objetoRep);
	};

	contarRepetidos(people);
}

/* problema4(); */

function problema5() {
	const myArray = [1, 2, 3, 0, 5, 8, 4];
	const nMayor = arreglo => Math.max(...arreglo);
	const nMenor = arreglo => Math.min(...arreglo);

	console.log(nMayor(myArray));
	console.log(nMenor(myArray));
}

/* problema5(); */

function problema6() {
	const miObjeto = {
		key1: 10,
		key2: 3,
		key3: 40,
		key4: 20,
		key5: 5,
	};

	const ordenaValor = objeto => {
		const valores = [];
		for (let [llave, valor] of Object.entries(objeto)) {
			valores.push(valor);
		}
		const arregloOrdenado = valores.sort((a, b) => a - b);
		const arregloFinal = [];
		let contador = 0;

		for (let llave of Object.entries(objeto)) {
			llave[1] = arregloOrdenado[contador];
			contador++;
			arregloFinal.push(llave);
		}
		console.log(arregloFinal);
	};

	ordenaValor(miObjeto);

	/* 
     for (let [llave, valor] of Object.entries(miObjeto)) {
            keys.push(llave);
            valores.push(valor);
        }
        const arregloOrdenado = valores.sort((a, b) => a - b);
        const arregloFinal = [];
        let contador = 0;
        for (let llave of Object.entries(miObjeto)) {
            llave[1] = arregloOrdenado[contador];
            contador++;
            arregloFinal.push(llave);
        }
        console.log(arregloFinal); */
}

problema6();
