function ejercicio1() {
	const anObject = {
		foo: 'bar',
		length: 'interesting',
		0: 'zero',
		1: 'one',
	};

	const anArray = ['zero.', 'one.'];

	console.log(anArray[0], anObject[0]);
	console.log(anArray[1], anObject[1]);
	console.log(anArray.length, anObject.length);
	console.log(anArray.foo, anObject.foo);
	console.log(
		typeof anArray == 'object',
		typeof anObject == 'object',
	);
	console.log(
		anArray instanceof Object,
		anObject instanceof Object,
	);
	console.log(Array.isArray(anArray), Array.isArray(anObject));
}

/* ejercicio1(); */

function ejercicio2() {
	const obj = {
		a: 'hello',
		b: 'this is',
		c: 'javascript',
	};

	const miArreglo = Object.values(obj);
	console.log(miArreglo);
}

/* ejercicio2(); */

function ejercicio3() {
	const serie = limite => {
		for (let i = 0; i <= limite * 2; i++) {
			i % 2 === 0 ? console.log(i) : null;
		}
	};

	serie(10);
}

/* ejercicio3(); */

function ejercicio4() {
	let zero = 0;
	const multiplica = x => x * 2;
	const add = (
		a = 1 + zero,
		b = a,
		c = b + a,
		d = multiplica(c),
	) => console.log(a + b + c, d);

	add(1);
	add(3);
	add(2, 7);
	add(1, 2, 5);
	add(1, 2, 5, 10);
}

/* ejercicio4(); */

function ejercicio5() {
	class MyClass {
		constructor() {
			this.names_ = [];
		}

		set name(value) {
			this.names_.push(value);
		}

		get name() {
			return this.names_[this.names_.length - 1];
		}
	}

	const myClassInstance = new MyClass();
	myClassInstance.name = 'Joe';
	myClassInstance.name = 'Bob';

	console.log(myClassInstance.name);
	console.log(myClassInstance.names_);
}

/* ejercicio5(); */

function ejercicio6() {
	const classInstance = new (class {
		get prop() {
			return 5;
		}
	})();

	classInstance.prop = 10;
	console.log(classInstance.prop);
}

/* ejercicio6(); */

function ejercicio7() {
	class Queue {
		constructor() {
			const list = [];
			this.enqueue = function (type) {
				list.push(type);
				return type;
			};
			this.dequeue = function () {
				return list.shift();
			};
		}
	}

	let q = new Queue();
	q.enqueue(9);
	q.enqueue(8);
	q.enqueue(7);
	console.log(q.dequeue());
	console.log(q.dequeue());
	console.log(q.dequeue());
	console.log(q);
	console.log(Object.keys(q));
}

/* ejercicio7(); */

function ejercicio8() {
	class Persona {
		#first;
		#last;
		constructor(first, last) {
			this.first = first;
			this.last = last;
		}

		get nombreCompleto() {
			return `${this.first} ${this.last}`;
		}
	}

	let person = new Persona('John', 'Lennon');
	console.log(person.nombreCompleto);
	person.first = 'Ringo';
	person.last = 'Star';
	console.log(person.nombreCompleto);
}

/* ejercicio8(); */

function ejercicio9() {
	let btnDelete = document.querySelectorAll(
		'[data-deletepost]',
	);

	btnDelete.forEach(btn =>
		btn.addEventListener('click', () => {
			const idDiv = btn.getAttribute('data-deletepost');
			const divAEliminar = document.querySelector(
				`#${idDiv}`,
			);
			let opc = confirm(
				'Seguro que quiere eliminar??? \nAceptar / Cancelar',
			);
			opc ? divAEliminar.remove() : null;
		}),
	);
}
/* ejercicio9(); */
