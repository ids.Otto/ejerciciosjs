function ejercicio1() {
	const body = document.querySelector('html');

	const cursorCor = document.querySelector('.cursor');
	const teclado = document.querySelector('.teclado');

	body.addEventListener('mousemove', e => {
		/* console.log(`[${e.clientX}, ${e.clientY}]`); */
		cursorCor.setAttribute(
			'style',
			`top: ${e.pageY}px; left: ${
				e.clientX - 15
			}px; display: block`,
		);
		cursorCor.textContent = `[${e.clientX}, ${e.clientY}]`;
	});

	body.addEventListener('keypress', e => {
		teclado.style.display = 'block';
		teclado.textContent = `Carácter: [ ${e.key} ]`;
		setTimeout(() => {
			teclado.style.display = 'none';
		}, 3000);
	});
}

ejercicio1();
